include $(MKDIR)/arm.mk

VPATH		+= $(SRCDIR)/arm/max
VPATH		+= $(SRCDIR)/common
#VPATH		+= $(SRCDIR)/common/micro-ecc

SRCS		+= bootloader.c
SRCS		+= clkman.c
SRCS		+= heap.c
SRCS		+= mxc_assert.c
SRCS		+= startup_max32625.S
SRCS		+= system_max32625.c
SRCS		+= util.S
SRCS		+= ioman.c
#SRCS		+= stdio.c
SRCS		+= flc.c
SRCS		+= crc.c
SRCS		+= update.c
SRCS		+= sha2.c
SRCS		+= lz4.c
SRCS        += debug.c

DEFS        += TARGET=MAX32625
DEFS        += TARGET_REV=0x4132
DEFS        += MAXIM32625
DEFS        += CFG_DEBUG
DEFS        += MXC_ASSERT_ENABLE
DEFS		+= LZ4_PAGEBUFFER_SZ=128
DEFS		+= UP_PAGEBUFFER_SZ=128

FLAGS		+= -mcpu=cortex-m4
FLAGS		+= -I$(SRCDIR)/common

#CFLAGS      += -mfloat-abi=softfp
#CFLAGS      += -mfpu=fpv4-sp-d16
CFLAGS      += -Wa,-mimplicit-it=thumb
CFLAGS      += -ffunction-sections
CFLAGS      += -fdata-sections
CFLAGS      += -MD
CFLAGS      += -Wno-format
CFLAGS		+= -Wall
CFLAGS		+= -Os
#CFLAGS		+= -O0
CFLAGS		+= -I$(SRCDIR)/arm/CMSIS/Device/Maxim/MAX32625/Include
#Lukas: This should make lst files
#CFLAGS      += -Wa,-adhln -g
CFLAGS		+= -Wa,-adhlns="$@.lst"

#Attention: LDFLAGS are set here for the first time, because arm.mk sets the option "nostartfiles"
#           and the MAXIM variant doesn't want to conflict with the basic Basicsmac variant.
#           That's the reason to redefine LDFLAGS coming from toolchain.mk.
LDFLAGS	= -Wl,--gc-sections -Wl,-Map,$(basename $@).map
LDFLAGS		+= -mcpu=cortex-m4
#LDFLAGS		+= -T$(SRCDIR)/arm/stm32lx/ld/STM32$(STM32_T)xx$(STM32_S).ld
#LDFLAGS		+= -T$(SRCDIR)/arm/stm32lx/ld/STM32$(STM32_T).ld
LDFLAGS     += -T$(SRCDIR)/arm/max/ld/max32625.ld
LDFLAGS     += -mthumb
#LDFLAGS     += -mfloat-abi=softfp
#LDFLAGS     += -mfpu=fpv4-sp-d16
#LDFLAGS     += -Xlinker --gc-sections

OBJS		= $(addsuffix .o,$(basename $(SRCS)))


BINPADDED	:= $(CROSS_COMPILE)objcopy -O binary --gap-fill 0xFF --pad-to 0x5000

# Include math library
#STD_LIBS=-lc -lm


bootloader: $(OBJS)

bootloader.hex: bootloader
	$(HEX) $< $@

bootloader.bin: bootloader
	$(BIN) $< $@

bootloader_padded.bin: bootloader	
	$(BINPADDED) $< $@

main: $(OBJS)

main.hex: main
	$(HEX) $< $@

default: bootloader.hex bootloader.bin bootloader_padded.bin

clean:
	rm -f *.o *.d *.map *.lst bootloader bootloader.hex bootloader.bin main main.hex

.PHONY: clean default


MAKE_DEPS       := $(MAKEFILE_LIST)     # before we include all the *.d files

-include $(OBJS:.o=.d)

$(OBJS): $(MAKE_DEPS)
